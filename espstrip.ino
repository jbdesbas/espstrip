#include <WiFiManager.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPUpdateServer.h>

#include <IRremoteESP8266.h>

WiFiManager wifiManager;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

IRrecv irrecv(5);
decode_results results;

#define RPIN 12
#define GPIN 13
#define BPIN 14

class Ledstrip
{
    byte rPin;
    byte gPin;
    byte bPin;

    public:
        String effect;
        int r;
        int g;
        int b;
        float bright;
        //ajouter un parametre SPEED
        Ledstrip(byte rPini,byte gPini,byte bPini){
            effect="color"; 
            r=0;
            g=0;
            b=0;
            bright=1;
            rPin=rPini;
            gPin=gPini;
            bPin=bPini;
            pinMode(rPin,OUTPUT);
            pinMode(gPin,OUTPUT);
            pinMode(bPin,OUTPUT);
        }
        void setEffect(String x){
            effect=x;
        }
        void setRGB(int ri, int gi, int bi){
            r=ri;b=bi,g=gi;
        }
        void setR(int x){
            r=x;
        }
        void setG(int x){
            g=x;
        }
        void setB(int x){
            b=x;
        }
        void setBright(float x){
            if(x>1){ bright=1;}
            else if(x<0){bright=0;}
            else{bright=x;}
        }
        void Handle(){
             int ro,go,bo;
             if(effect=="police"){
                if((millis() % 2000 ) >= 1000){
                    ro=1023;
                    go=0;
                    bo=0;
                }else{
                    ro=0;
                    go=0;
                    bo=1023;
                }
            }else if (effect=="color"){
                ro=r;go=g;bo=b;
            }else{
                ro=0;bo=0;go=0;
            }

            analogWrite(rPin,ro*bright);
            analogWrite(gPin,go*bright);
            analogWrite(bPin,bo*bright);
        }
};


Ledstrip lds(RPIN,GPIN,BPIN);
unsigned long last_trame;

void setup() {
    Serial.begin(9600);
    wifiManager.autoConnect("espStrip");
    httpUpdater.setup(&server);
    lds.setRGB(0,0,0);
    lds.setEffect("color");
    lds.setBright(1);

    server.on("/", httpRoot);
    server.on("/light",httpLight);
    server.begin();

    irrecv.enableIRIn();
}

void loop() {
    server.handleClient();
    lds.Handle();
    HandleIR();
}

void httpRoot(){
    String str="last IR trame : ";
    str+=String(last_trame,HEX);
    Serial.println(last_trame);
    server.send(200,"text/html",str);
}

void httpLight(){
    if(server.arg("effect")!=NULL){lds.setEffect(server.arg("effect")); }
    if(server.arg("r")!=NULL){lds.setR(server.arg("r").toInt()); }
    if(server.arg("g")!=NULL){lds.setG(server.arg("g").toInt()); }
    if(server.arg("b")!=NULL){lds.setB(server.arg("b").toInt()); }
    if(server.arg("bright")!=NULL){lds.setBright(server.arg("bright").toFloat()); }

    String str=lds.effect; //A sortir en json/jsonp ?
    str+="<br>R: ";
    str+=String(lds.r);
    str+="<br>G: ";
    str+=String(lds.g);
    str+="<br>B: ";
    str+=String(lds.b);
    str+="<br> Bright: ";
    str+=String(lds.bright);
    server.send(200,"text/html",str);
}

void HandleIR(){
    if (irrecv.decode(&results)) {
        Serial.println(results.value, HEX);
        last_trame=results.value;

        switch(last_trame){ 
            case 0x20DF4EB1 : //LGrouge
                lds.setEffect("color");lds.setRGB(1023,0,0); break;
            case 0x20DF8E71 : //LGvert
                lds.setEffect("color");lds.setRGB(0,1023,0); break;
            case 0x20DFC639 : //LGjaune
                lds.setEffect("color");lds.setRGB(1023,1023,0); break;
            case 0x20DF8679 : //LGbleu
                lds.setEffect("color");lds.setRGB(0,0,1023); break;
            case 0xff3ac5 : //bright_up
                lds.setBright(lds.bright+0.1); break;
            case 0xffba45 : //bright_down
                lds.setBright(lds.bright-0.1); break;
        }
        delay(150);
        irrecv.resume(); // Receive the next value
    }
}
